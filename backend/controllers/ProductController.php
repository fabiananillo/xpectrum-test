<?php

namespace backend\controllers;

use Yii;
use common\models\Product;
use common\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\ProductSubcategory;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post())) {
            $dataPost = Yii::$app->request->post('Product');

            $model->product_category_id = $dataPost['product_category_id'];
            $model->price = preg_replace("/[^0-9\.]+/", "", $dataPost['price']);
            $photos_url = UploadedFile::getInstancesByName('photos');

            if (!empty($photos_url)) {
                $model->photos_url = $this->uploadProductPhotos($photos_url, $model);
            }
            if ($model->validate() && empty($model->getErrors())) {
                Yii::$app->session->setFlash("success", "Producto creado con éxito.");
                $model->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    private function uploadProductPhotos($photos_url, $model)
    {
        $photoUploaded = array();
        foreach ($photos_url as $photo) {

            $randomName = Yii::$app->security->generateRandomString(5);
            $path = Yii::getAlias('@root') . '/frontend/web/temp_files/';
            $photo->name = $randomName . '.' . $photo->extension;
            $photo->saveAs($path . $photo->name);

            //save 1366x768
            Yii::$app->xpectrum->scaleImage($photo, $path, 'products/1200x1125/', 1366, 768, 100);

            if (array_push($photoUploaded, Yii::$app->xpectrum->uploadImage($path . 'products/1200x1125/' . $photo->name))) {
                /* Deletes the temp file */
                unlink($path . $photo);
                unlink($path . 'products/1200x1125/' . $photo->name);
            }
        }

        return $photoUploaded;
    }
    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $dataPost = Yii::$app->request->post('Product');
            echo'<pre>';
            print_r($dataPost);
            exit();
            $model->photos_url = $model->photos_url;
            $model->price = preg_replace("/[^0-9\.]+/", "", $dataPost['price']);
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash("success", "Se ha eliminado el producto.");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionSubcategory($id)
    {

        $cuentaDpto = ProductSubcategory::find()->where(['product_category_id' => $id])
            ->count();
        $departamentos = ProductSubcategory::find()->where(['product_category_id' => $id])
            ->all();
        if ($cuentaDpto > 0) {
            echo "<option value='' disabled selected>Por favor seleccione" . "</option>";
            foreach ($departamentos as $departamento) {
                echo "<option value='" . $departamento->id . "'>" . $departamento->product_subcategory_name . "</option>";
            }
        } else {
            echo "<option> - </option>";
        }
    }

    public function beforeAction($action)
    {
        if ($action->id == 'subcategory') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
}
