<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            $dataPost = Yii::$app->request->post('User');
            $model->first_name = $dataPost['first_name'];
            $model->last_name = $dataPost['last_name'];
            $model->email = $dataPost['email'];
            $model->username = $dataPost['email'];
            $model->address = $dataPost['address'];
            $model->phone_code = '+57';
            $model->phone_number = $dataPost['phone_number'];
            $model->password_hash = Yii::$app->security->generatePasswordHash($dataPost['password_hash']);
            $model->status = User::STATUS_ACTIVE;
            $model->save();

            if($model->validate() && empty($model->getErrors())) {
                $model->save();
                Yii::$app->session->setFlash("success", "Usuario creado con éxito.");
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash("error", "Ha ocurrido un error, revisa.");
            }
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $dataPost = Yii::$app->request->post('User');
            $model->first_name = $dataPost['first_name'];
            $model->last_name = $dataPost['last_name'];
            $model->email = $dataPost['email'];
            $model->username = $dataPost['email'];
            $model->address = $dataPost['address'];
            $model->phone_code = '+57';
            $model->phone_number = $dataPost['phone_number'];
            $model->password_hash = Yii::$app->security->generatePasswordHash($dataPost['password_hash']);
            $model->status = User::STATUS_ACTIVE;

            if($model->validate() && empty($model->getErrors())) {
                $model->update();
                Yii::$app->session->setFlash("success", "Usuario actualizado con éxito.");
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash("error", "Ha ocurrido un error, revisa.");
            }
            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
