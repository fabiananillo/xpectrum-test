<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ProductCategory;
/* @var $this yii\web\View */
/* @var $model common\models\ProductSubcategory */

$this->title = 'Detalles: ' . $model->product_subcategory_name;
$this->params['breadcrumbs'][] = ['label' => 'Subcategoría de Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-subcategory-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quiere borrar esto?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Ir a Lista', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'product_category_id',
                'label' => 'Categoría del Producto',
                'value' => function($model) {
                    return ProductCategory::find()->select('product_category_name')->where(['id' => $model->product_category_id])->one()->product_category_name;
                }
            ],
            [
                'attribute' => 'product_subcategory_name',
                'label' => 'Subcategoría del Producto',
            ],
        ],
    ]) ?>

</div>
