<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\ProductCategory;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\ProductSubcategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-subcategory-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
        $form->field($model, 'product_category_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(ProductCategory::find()->all(), 'id', 'product_category_name'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione categoría del producto'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('Categoría del Producto');
    ?>

    <?= $form->field($model, 'product_subcategory_name')->textInput()->label('Nombre de la subcategoría'); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>