 <!-- Small boxes (Stat box) -->
 <div class="row">
     <div class="col-lg-3 col-xs-6">
         <!-- small box -->
         <div class="small-box bg-aqua">
             <div class="inner">
                 <h3><?= $totalOrder;?></h3>

                 <p>Compras</p>
             </div>
             <div class="icon">
                 <i class="ion ion-bag"></i>
             </div>
         </div>
     </div>
     <!-- ./col -->
     <div class="col-lg-3 col-xs-6">
         <!-- small box -->
         <div class="small-box bg-green">
             <div class="inner">
                 <h3><?= $totalProduct; ?></h3>

                 <p>Productos Registrados</p>
             </div>
             <div class="icon">
                 <i class="ion ion-stats-bars"></i>
             </div>
            
         </div>
     </div>
     <!-- ./col -->
     <div class="col-lg-3 col-xs-6">
         <!-- small box -->
         <div class="small-box bg-yellow">
             <div class="inner">
                 <h3><?= $totalUser; ?></h3>

                 <p>Usuarios Registrados</p>
             </div>
             <div class="icon">
                 <i class="ion ion-person-add"></i>
             </div>
             
         </div>
     </div>
     <!-- ./col -->
 </div>
 <!-- /.row -->