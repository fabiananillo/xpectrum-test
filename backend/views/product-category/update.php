<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductCategory */

$this->title = 'Actualizar Categoría: ' . $model->product_category_name;
$this->params['breadcrumbs'][] = ['label' => 'Categoría de Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_category_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
