<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Agregar Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'first_name',
                'label' => 'Primer Nombre',
            ],
            [
                'attribute' => 'last_name',
                'label' => 'Segundo Nombre',
            ],
            [
                'attribute' => 'phone_code',
                'label' => 'Indicativo',
            ],
            [
                'attribute' => 'phone_number',
                'label' => 'Teléfono'
            ],
            [
                'attribute' => 'email',
                'label' => 'Correo Electrónico'
            ],
            //'username',
            //'address',
            //'password_hash',
            //'auth_key',
            //'password_reset_token',
            //'status',
            [
                'attribute' => 'created_at',
                'label' => 'Fecha Creación',
            ],
            [
                'attribute' => 'updated_at',
                'label' => 'Fecha Actualizado',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
