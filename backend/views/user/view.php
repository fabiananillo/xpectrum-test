<?php

use common\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Usuario: '.$model->first_name.' '.$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Desea eliminar este usuario?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Ir a Lista', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'first_name',
            'last_name',
            'phone_code',
            'phone_number',
            'email:email',
            'username',
            'address',
            //'password_reset_token',
            [
                'attribute' => 'status',
                'label' => 'Estado',
                'value' => function($model) {
                    return $model->status == User::STATUS_ACTIVE ? 'Activo' : 'Inactivo';
                }
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
