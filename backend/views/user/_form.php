<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'first_name')->textInput(['autofocus' => true])->label('Primer Nombre'); ?>
    <?= $form->field($model, 'last_name')->textInput()->label('Segundo Nombre'); ?>
    <?= $form->field($model, 'address')->textInput()->label('Dirección'); ?>
    <?= $form->field($model, 'phone_number')->textInput()->label('Teléfono'); ?>
    <?= $form->field($model, 'email')->textInput()->label('Correo Electrónico'); ?>
    <?= $form->field($model, 'password_hash')->passwordInput()->label('Contraseña'); ?>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>