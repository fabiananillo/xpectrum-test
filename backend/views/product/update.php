<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = 'Actualizar: ' . $model->product_name;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$this->registerJsFile(
    '@web/js/manageCurrency.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin(); ?>
                <div class="col-lg-5 col-md-12 col-sm-12">
                    <?= $form->field($model, 'product_name')->textInput()->label('Nombre del Producto') ?>
                </div>
                <?php
                //     $form->field($model, 'product_category_id')->widget(Select2::classname(), [
                //     'data' => ArrayHelper::map(ProductCategory::find()->all(), 'id', 'product_category_name'),
                //     'language' => 'es',
                //     'options' => ['placeholder' => 'Seleccione categoría del producto'],
                //     'pluginOptions' => [
                //         'allowClear' => true
                //     ],
                // ])->label('Categoría del Producto');
                ?>

                <div class="col-lg-5 col-md-12 col-sm-12">
                    <?= $form->field($model, 'price')->textInput(['id' => 'input-price'])->label('Precio') ?>
                </div>
                <?= Html::hiddenInput('output-price', null, ['id' => 'output-price']) ?>
                <div class="col-lg-10 col-md-12 col-sm-12">
                    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('Descripción') ?>
                </div>
                <div class="col-lg-10 col-md-12 col-sm-12">
                    <?= $form->field($model, 'in_sell')->checkbox(['checked' => true]); ?>
                </div>
                <div class="col-lg-10 col-md-12 col-sm-12">
                    <?= $form->field($model, 'draft')->checkbox(['checked' => false]); ?>
                </div>
                <div class="col-lg-10 col-md-12 col-sm-12">
                    <div class="form-group">
                        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>