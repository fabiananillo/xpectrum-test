<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ProductCategory;
use common\models\ProductSubcategory;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(
    '@web/js/manageCurrency.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']

            ]); ?>
            <div class="col-lg-5 col-md-12 col-sm-12">
                <?= $form->field($model, 'product_name')->textInput()->label('Nombre del Producto') ?>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12">
                <?= $form->field($model, 'price')->textInput(['id' => 'input-price'])->label('Precio') ?>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12">
                <?=
                    $form->field($model, 'product_category_id')->dropDownList(ArrayHelper::map(ProductCategory::find()->all(), 'id', 'product_category_name'), [
                        'onchange' => '$.post( "' . Yii::$app->urlManager->createAbsoluteUrl(['product/subcategory?id='], 'https') . '"+$(this).val(),function( data ){ 
                                    $( "select#order-state-form" ).html( data );
                                });',
                        'class' => 'form-control',
                        'id' => 'category_id',
                        'value' => 1

                    ])->label('Categoría del Producto');
                ?>

            </div>

            <div class="col-lg-5 col-md-12 col-sm-12">
                <?=
                    $form->field($model, 'product_subcategory_id')->dropDownList(ArrayHelper::map(ProductSubcategory::find()->where(['product_category_id' => 1])->all(), 'id', 'product_subcategory_name'), [
                        'id' => 'order-state-form',
                        'value' => 1,
                    ])->label('Subcategoría del Producto')
                ?>

            </div>
            <?= Html::hiddenInput('output-price', null, ['id' => 'output-price']) ?>
            <div class="col-lg-10 col-md-12 col-sm-12">
                <?= $form->field($model, 'description')->textarea([
                    'rows' => 6,
                    'style' => 'resize:none;'
                ])->label('Descripción') ?>
            </div>
            <div class="col-lg-10">
                <h3>Subir Fotos</h3>
                <?= FileInput::widget([
                    'model' => $model,
                    'name' => 'photos',
                    'options' => [
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'maxFileCount' => 4,
                        'showRemove' => false,
                        'showUpload' => false,
                    ]
                ]);
                ?>
            </div>
            <div class="col-lg-10 col-md-12 col-sm-12">
                <?= $form->field($model, 'in_sell')->checkbox(['checked' => true]); ?>
            </div>
            <div class="col-lg-10 col-md-12 col-sm-12">
                <div class="form-group">
                    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>