<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ProductCategory;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Agregar Producto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'product_category_id',
                'value' => function($model) {
                    $productCategoryName = (new Query())
                        ->select('*')
                        ->from('product_category')
                        ->where(['id' => $model->product_category_id])
                        ->one()['product_category_name'];
                    return $productCategoryName;
                },
                'label' => 'Categoría del Producto',
                'format'    => 'raw',
                'filter'    => Select2::widget([
                                'model' => $searchModel,
                                'attribute' => 'product_category_id',
                                'data' => ArrayHelper::map(ProductCategory::find()->all(), 'id', 'product_category_name'),
                                'options' => ['placeholder' => 'Seleccione...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]),
            ],
            'product_name',
            //'description:ntext',
            [
                'attribute' => 'price',
                'value' => function($model) {
                    return '$'.number_format($model->price, 0);
                }
            ],
            //'photos_url:url',
            //'in_sell:boolean',
            [
                'attribute' => 'in_sell',
                'label' => 'Disponibilidad',
                'value' => function($model) {
                    return $model->in_sell == true ? 'Sí' : 'No';
                }
            ],
            //'total_score',
            //'seo_slug',
            //'created_by',
            //'created_at',
            //'updated_by',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
