<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use  yii\db\Query;
/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->product_name;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<h1><?= Html::encode($this->title) ?></h1>

<p>
    <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => '¿Estás seguro que quiere borrar esto?',
            'method' => 'post',
        ],
    ]) ?>
    <?= Html::a('Ir a Lista', ['index'], ['class' => 'btn btn-success']) ?>
</p>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        [
            'attribute' => 'product_category_id',
            'value' => function ($model) {
                $productCategoryName = (new Query())
                    ->select('*')
                    ->from('product_category')
                    ->where(['id' => $model->product_category_id])
                    ->one()['product_category_name'];
                return $productCategoryName;
            },
        ],
        'product_name',
        [
            'attribute' => 'price',
            'value' => function ($model) {
                return '$' . number_format($model->price, 0);
            }
        ],
        'description:ntext',
        [
            'attribute' => 'photos_url',
            'format' => 'raw',
            'value' => function ($model) {
                $photos = null;
                foreach ($model->photos_url as $key => $value) {
                    $photos .= Html::img($value, ['height' => 80, 'width' => 80]) . ' ';
                }

                return $photos;
            }
        ],
        'in_sell:boolean',
        //'draft:boolean',
        //'total_score',
        'seo_slug',
        [
            'attribute' => 'created_by',
            'value' => function ($model) {
                $adminName = (new Query())
                    ->select('username')
                    ->from('admin')
                    ->where(['id' => $model->created_by])
                    ->one()['username'];
                return $adminName;
            },
        ],
        'created_at',
        [
            'attribute' => 'updated_by',
            'value' => function ($model) {
                $adminName = (new Query())
                    ->select('username')
                    ->from('admin')
                    ->where(['id' => $model->updated_by])
                    ->one()['username'];
                return $adminName;
            },
        ],
        'updated_at',
    ],
]) ?>