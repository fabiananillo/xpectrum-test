<?php

use common\models\Payment;
use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Compras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'payment_id',
                'label' => 'Estado del Pago',
                'value' => function ($model) {
                    $payment = Payment::findOne(['id' => $model->payment_id]);
                    return $payment->status;
                }
            ],
            [
                'label' => 'Método de Pago',
                'value' => function ($model) {
                    $payment = Payment::findOne(['id' => $model->payment_id]);
                    return $payment->transfer_method;
                }
            ],
            [
                'attribute' => 'first_name',
                'label' => 'Nombre',
            ],
            [
                'attribute' => 'last_name',
                'label' => 'Apellido',
            ],
            [
                'attribute' => 'user_id',
                'label' => 'Correo Usuario',
                'value' => function ($model) {
                    return $model->email;
                }
            ],

            [
                'attribute' => 'total_payment',
                'label' => 'Total Pago',
                'value' => function ($model) {
                    return $model->total_payment;
                }
            ],
            [
                'attribute' => 'invoice',
                'label' => 'Factura',
                'format' => 'raw',
                'filter' => false,
                'value' => function ($model) {
                    return Html::a('Descargar', Yii::getAlias('@web').'/invoices/'.$model->invoice, ['download' => true]);
                }
            ],
            //'first_name',
            //'last_name',
            //'country_id',
            //'state_id',
            //'city',
            //'address',
            //'apartment',
            //'zip_code',
            //'email:email',
            //'phone',
            //'order_notes',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>