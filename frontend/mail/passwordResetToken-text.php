<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = $user->password_reset_token;
?>
Hola <?= $user->first_name. ' ' . $user->last_name;  ?>,

Recientemente ha solicitado recuperar sus credenciales
para proceder con la solicitud, deberá hacer click en
el siguiente enlace:

<?= $resetLink ?>

Si cree que no ha sido usted quien ha solicitado este cambio
por favor ignorar este mensaje.