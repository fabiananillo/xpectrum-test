<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['clave-nueva', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Hola <?= Html::encode($user->first_name. ' ' . $user->last_name); ?>,</p>

    <p>Recientemente ha solicitado recuperar su contraseña <br>
       para proceder con la solicitud, deberá hacer click en
       el siguiente enlace:
    </p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    
     <p>Si cree que no ha sido usted quien ha solicitado este cambio
         por favor ignorar este mensaje.
    </p>
</div>
