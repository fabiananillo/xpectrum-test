<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Product;
use common\models\Payment;
use common\models\Order;
use common\models\Countries;
use yii\helpers\ArrayHelper;
use common\models\States;
use common\models\OrderProductPayment;
use common\models\OrderProductDetail;
use Konekt\PdfInvoice\InvoicePrinter;
use yii\helpers\FileHelper;

class OrderController extends Controller
{
    /**
     * @var \devanych\cart\Cart $cart
     */
    private $cart;
    const payment_key = '8647ff689e2445816dc383d758bfe85ee0cc3e44';
    const customer_id = '33709';

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->cart = Yii::$app->cart;
    }

    public function actionGenerate()
    {
        if (Yii::$app->user->isGuest) {
            $session = Yii::$app->session; // inicializa una variable de sesión. Los siguientes usos son equivalentes:
            $session->set('url', Yii::$app->request->url);
            Yii::$app->session->setFlash('warning', 'Debes tener una cuenta para comprar. Por favor, accede.');
            return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(['ingreso']));
        }
        $model = new order();
        $countries = Countries::getAll();
        $defaultStates = ArrayHelper::map(States::find()->where(['country_id' => Countries::COLOMBIA_ID])->all(), 'id', 'name');
        $cart = $this->cart;
        //if cart its empty
        if (empty($cart->getItems())) {
            Yii::$app->session->setFlash('error', 'Tu carro de compras está vacío.');
            return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(['tienda']));
        }


        return $this->render('generate', [
            'cart' => $cart,
            'countries' => $countries,
            'defaultStates' => $defaultStates,
            'model' => $model,
        ]);
    }

    public function actionPayment()
    {
        $cart = $this->cart;
        if (Yii::$app->request->post() && !empty($cart->getItems())) {
            $arrayProduct = array();
            $cartItems = $cart->getItems();
            $dataPost = Yii::$app->request->post('Order');

            $orderProductPayment = new OrderProductPayment();
            foreach ($cartItems as $item) {
                $product = $item->getProduct();
                $cost = $item->getCost();
                $quantity = $item->getQuantity();
                array_push($arrayProduct, ['product' => $product->id, 'cost' => $cost, 'quantity' => $quantity]);
            }
            $orderProductPayment->product_json = json_encode($arrayProduct, true);
            $orderProductPayment->country_id = $dataPost['country_id'];
            $orderProductPayment->state_id = $dataPost['state_id'];
            $orderProductPayment->city = $dataPost['city'];
            $orderProductPayment->address = $dataPost['address'];
            $orderProductPayment->apartment = $dataPost['apartment'];
            $orderProductPayment->zip_code = $dataPost['zip_code'];
            $orderProductPayment->phone = $dataPost['phone'];
            $orderProductPayment->order_notes = $dataPost['order_notes'];

            $orderProductPayment->save();
            return $this->render('payment', [
                'cart' => $this->cart,
                'dataPost' => $dataPost,
                'orderProductPaymentId' => $orderProductPayment->getPrimaryKey(),
            ]);
        } else {
            Yii::$app->session->setFlash('warning', '¡Oops! Algo ha salido mal, intenta de nuevo.');
            return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(['orden/generar']));
        }
    }

    public function actionTransaction($ref_payco)
    {

        $cart = $this->cart;
        $cart->clear();
        /*Usamos la api de ePayco y obtenemos el objeto JSON decodificandolo al mismo tiempo, y se concatena la referencia que viene por el pago confirmacion*/
        $dataPayco = \yii\helpers\Json::decode(file_get_contents('https://secure.epayco.co/validation/v1/reference/' . $ref_payco));
        // echo'<pre>';
        // print_r($dataPayco);
        // exit();

        return $this->render('transaction', ['dataPayco' => $dataPayco['data']]);
    }

    public function actionConfirmation()
    {
        /* En esta página se reciben las variables enviadas desde ePayco hacia el servidor.
          Antes de realizar cualquier movimiento en base de datos se deben comprobar algunos valores
          Es muy importante comprobar la firma enviada desde ePayco
          Ingresar  el valor de p_cust_id_cliente lo encuentras en la configuración de tu cuenta ePayco
          Ingresar  el valor de p_key lo encuentras en la configuración de tu cuenta ePayco
         */

        $cart = $this->cart;
        $cart->clear();
        $dataGet = Yii::$app->request->get();
        $p_cust_id_cliente = self::customer_id;
        $p_key = self::payment_key;
        $x_ref_payco = $dataGet['x_ref_payco'];
        $x_transaction_id = $dataGet['x_transaction_id'];
        $x_amount = $dataGet['x_amount'];
        $x_currency_code = $dataGet['x_currency_code'];
        $x_signature = $dataGet['x_signature'];
        $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);
        $x_response = $dataGet['x_respuesta'];

        //Validamos la firma
        if ($x_signature == $signature) {
            /* Si la firma esta bien podemos verificar los estado de la transacción */
            $x_cod_response = $dataGet['x_cod_response'];

            switch ((int) $x_cod_response) {
                case 1:
                    $payment = new Payment();
                    $payment->concept = 'Compra';
                    $payment->transfer_method = $dataGet['x_bank_name'];
                    $payment->cardnumber = $dataGet['x_cardnumber'];
                    $payment->value = '$' . number_format($dataGet['x_amount'], 0);
                    $payment->status = $x_response;
                    $payment->doc_type = $dataGet['x_customer_doctype'];
                    $payment->document = $dataGet['x_customer_document'];
                    $payment->name = $dataGet['x_customer_name'];
                    $payment->last_name = $dataGet['x_customer_lastname'];
                    $payment->email = $dataGet['x_customer_email'];
                    $payment->ip_address = $dataGet['x_customer_ip'];
                    $payment->epayco_ref = $x_ref_payco;
                    $payment->order_product_payment_id = $dataGet['x_extra1'];
                    $payment->save() ? $this->createNewOrder($dataGet, $payment->getPrimaryKey()) : null;
                    break;
            }
        } else {
            die("Firma no valida");
        }
    }

    private function createNewOrder($dataGet, $paymentId)
    {
        $orderProductPayment = OrderProductPayment::findOne(['id' => $dataGet['x_extra1']]);

        $items = json_decode($orderProductPayment->product_json, true);
        $invoice = $this->generateNewInvoice($dataGet, $items, $paymentId);
        $order = new Order();
        $order->payment_id = $paymentId;
        $order->user_id = $dataGet['x_extra3'];
        $order->total_payment = '$' . number_format($dataGet['x_amount'], 0);
        $order->invoice = $invoice;
        $order->first_name = $dataGet['x_customer_name'];
        $order->last_name = $dataGet['x_customer_lastname'];
        $order->country_id = $orderProductPayment->country_id;
        $order->state_id = $orderProductPayment->state_id;
        $order->city = $orderProductPayment->city;
        $order->address = $orderProductPayment->address;
        $order->zip_code = $orderProductPayment->zip_code;
        $order->email = $dataGet['x_customer_email'];
        $order->phone = $orderProductPayment->phone;
        $order->order_notes = $orderProductPayment->order_notes;
        if ($order->save()) {
            $this->setOrderProductDetail($order->getPrimaryKey(), $items);
            $this->sendEmailConfirmation($dataGet, $invoice);
        }
    }

    private function generateNewInvoice($dataGet, $items, $paymentId)
    {

        $invoice = new InvoicePrinter('A4', '$', 'es');
        $nameInvoice = Yii::$app->security->generateRandomString(5) . date('Y-m-dH:i:s') . '.pdf';
        $frontendPath = Yii::getAlias('@root') . '/frontend/web/temp_files/invoices/';
        $adminPath = Yii::getAlias('@root') . '/admin/invoices/';
        $invoice->setLogo("https://res.cloudinary.com/fabiananillo/image/upload/v1572480016/logo_pyty5a.png");   //logo image path
        $invoice->setColor("#34e079");      // pdf color scheme
        $invoice->setType("Factura de Venta");    // Invoice Type
        $invoice->setReference("FV-" . $paymentId);   // Reference
        $invoice->setDate(date('M dS ,Y', time()));   //Billing Date
        $invoice->setTime(date('h:i:s A', time()));   //Billing Time
        $invoice->setFrom(array("Vendedor", "Xpectrum Test", "Cl 83 # 79 - 18", "Barranquilla, Atlántico"));
        $invoice->setTo(array("Comprador", $dataGet['x_customer_name'] . " " . $dataGet['x_customer_lastname'], "128 AA Juanita Ave", "Glendora , CA 91740"));
        foreach ($items as $item) {
            $product = $this->getProduct($item['product']);
            $invoice->addItem($product->product_name, $product->description, $item['quantity'], 0, $product->price, 0, $item['cost']);
        }
        $invoice->addTotal("Sub Total", $dataGet['x_amount']);
        $invoice->addTotal("Total a Pagar", $dataGet['x_amount'], true);
        $invoice->addBadge("Pagado");
        $invoice->addTitle("Importante");
        $invoice->addParagraph("Garantizado");
        $invoice->setFooternote("Xpectrum Test");
        $invoice->render($frontendPath . $nameInvoice, 'F');
        chmod($frontendPath . $nameInvoice, 0755);
        copy($frontendPath . $nameInvoice, $adminPath . $nameInvoice);
        return $nameInvoice;
    }

    private function sendEmailConfirmation($dataGet, $nameInvoice)
    {
        return Yii::$app->mailer->compose()
            ->setTo($dataGet['x_customer_email'])
            ->setFrom('fabiananillo@gmail.com')
            ->setSubject('Factura Xpectrum Test')
            ->setTextBody('Gracias por tu compra. Anexamos tu factura.')
            ->attach(Yii::getAlias('@root') . '/frontend/web/temp_files/invoices/' . $nameInvoice)
            ->send();
    }

    private function setOrderProductDetail($orderId, $items)
    {

        foreach ($items as $item) {
            $orderProductDetail = new OrderProductDetail();
            $product = $this->getProduct($item['product']);
            $orderProductDetail->product_id = $product->id;
            $orderProductDetail->order_id = $orderId;
            $orderProductDetail->quantity = $item['quantity'];
            $orderProductDetail->total_price = '$' . number_format($item['cost'], 0);
            $orderProductDetail->save();
        }
    }

    /**
     * @param integer $id
     * @return Product the loaded model
     * @throws \DomainException if the product cannot be found
     */
    private function getProduct($id)
    {
        if (($product = Product::findOne((int) $id)) !== null) {
            return $product;
        }
        throw new \DomainException('Producto no encontrado');
    }


    public function redirect($url, $statusCode = 302)

    {

        \Yii::$app->session->close();

        return parent::redirect($url, $statusCode);
    }

    public function beforeAction($action)
    {
        if ($action->id == 'confirmation') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
}
