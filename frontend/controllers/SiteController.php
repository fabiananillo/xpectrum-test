<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\States;
use common\models\Product;
use common\models\ProductSubcategory;
use yii\data\Pagination;
use common\models\Countries;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $searchedParams = array();

        $query = Product::find()->where(['draft' => false]);
        $countQuery = clone $query;
        $totalProductsCount = $countQuery->count();
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 6]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $queryParams = Yii::$app->request->get();
        if (Yii::$app->request->get() && isset($queryParams['search_marketplace'])) {

            $query = Product::find()
                ->orderBy('id desc');

            //This searchedParams is for save the filter data from input client
            //sends it to view in array, if empty true, declare a new empty array
            $searchedParams = [
                'product_name' => $queryParams['product_name'],
            ];

            if (!empty($queryParams['product_name'])) {
                $query->orWhere(['ilike', 'product.seo_slug', strtolower($queryParams['product_name'])]);
            }
            $query->andWhere(['draft' => false]);
            $countQuery = clone $query;
            $totalProductsCount = $countQuery->count();
            $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 6]);
            $products = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        }
        return $this->render('index', [
            'products' => $products,
            'pages' => $pages,
            'searchedParams' => $searchedParams,
            'totalProductsCount' => $totalProductsCount,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $session = Yii::$app->session;
        $url = $session->get('url');

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->session->setFlash("success", "Bienvenido " . Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name);
            if (!empty($url)) {
                $session->remove('url');
                return $this->redirect($url);
            } else {
                return $this->goBack();
            }
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        Yii::$app->session->setFlash('success', 'Se ha cerrado su sesión.');
        return $this->goHome();
    }


    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $session = Yii::$app->session;
        $url = $session->get('url');

        $model = new SignupForm();
        $countries = Countries::getAll();
        $defaultStates = ArrayHelper::map(States::find()->where(['country_id' => Countries::COLOMBIA_ID])->all(), 'id', 'name');
        if ($model->load(Yii::$app->request->post())) {
            $model->validate();
            if (empty($model->getErrors())) {
                $user = $model->signup();
                if (Yii::$app->getUser()->login($user)) {
                    Yii::$app->session->setFlash("success", "Bienvenido " . $user->first_name . ' ' . $user->last_name);
                    if (!empty($url)) {
                        $session->remove('url');
                        return $this->redirect($url);
                    } else {
                        return $this->goBack();
                    }
                }
            } else {
                Yii::$app->session->setFlash("error", "Han ocurrido errores. Por favor, revisa.");
            }
        }


        return $this->render('signup', [
            'model' => $model,
            'countries' => $countries,
            'defaultStates' => $defaultStates,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (empty($model->getErrors())) {
                $model->sendEmail();
                Yii::$app->session->setFlash('success', 'Revisa la bandeja de tu correo electrónico y sigue las instrucciones.');
            } else {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error. Por favor, revisa.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionNewPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Se ha modificado su contraseña con éxito.');
            return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(['ingreso']));
        }


        return $this->render('new-password', [
            'model' => $model,
        ]);
    }

    public function actionState($id)
    {

        $cuentaDpto = States::find()->where(['country_id' => $id])
            ->count();
        $departamentos = States::find()->where(['country_id' => $id])
            ->all();
        if ($cuentaDpto > 0) {
            echo "<option value='' disabled selected>Por favor seleccione" . "</option>";
            foreach ($departamentos as $departamento) {
                echo "<option value='" . $departamento->id . "'>" . $departamento->name . "</option>";
            }
        } else {
            echo "<option> - </option>";
        }
    }

    public function actionSubcategory($id)
    {

        $cuentaDpto = ProductSubcategory::find()->where(['product_category_id' => $id])
            ->count();
        $departamentos = ProductSubcategory::find()->where(['product_category_id' => $id])
            ->all();
        if ($cuentaDpto > 0) {
            echo "<option value='' disabled selected>Por favor seleccione" . "</option>";
            foreach ($departamentos as $departamento) {
                echo "<option value='" . $departamento->id . "'>" . $departamento->product_subcategory_name . "</option>";
            }
        } else {
            echo "<option> - </option>";
        }
    }

    public function beforeAction($action)
    {
        if ($action->id == 'subcategory') {
            $this->enableCsrfValidation = false;
        }
        if ($action->id == 'state') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
}
