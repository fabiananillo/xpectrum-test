<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Product;


class CartController extends Controller
{
    /**
     * @var \devanych\cart\Cart $cart
     */
    private $cart;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->cart = Yii::$app->cart;
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'cart' => $this->cart,
        ]);
    }

    public function actionAdd()
    {
        if ($dataPost = Yii::$app->request->post()) {
            try {
                $product = $this->getProduct($dataPost['product-id']);
                
                $quantity = ltrim(preg_replace('/\D/', "", preg_replace("/[^0-9]/", "",$dataPost['quantity'])), '0');

                if (empty($quantity) || $quantity > 99 || $quantity == 0) {
                    Yii::$app->session->setFlash('warning', 'Por favor, ingresa la cantidad correctamente.');
                } else {
                    $this->cart->add($product, $quantity);
                    Yii::$app->session->setFlash('success', 'Se ha agregado al carrito.');
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(['carro']));
    }

    public function actionChange($productId, $quantity)
    {
        try {
            $product = $this->getProduct($productId);
            $quantity = preg_replace("/[^0-9\.]+/", "", $quantity);
            if ($item = $this->cart->getItem($product->id)) {
                if (empty($quantity) || $quantity > 99 || $quantity == 0) {
                    Yii::$app->session->setFlash('warning', 'Por favor, ingresa la cantidad correctamente.');
                } else {
                    $this->cart->change($item->getId(), $quantity);
                }
            }
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(['carro']));
    }

    public function actionRemove($id)
    {
        try {
            $product = $this->getProduct($id);
            $this->cart->remove($product->id);
            Yii::$app->session->setFlash('success', 'Se ha removido el producto con éxito.');
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    public function actionClear()
    {
        $this->cart->clear();
        Yii::$app->session->setFlash('success', 'Se limpiado tu carro de compras.');
        return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(['carro']));
    }

    /**
     * @param integer $id
     * @return Product the loaded model
     * @throws \DomainException if the product cannot be found
     */
    private function getProduct($id)
    {
        if (($product = Product::findOne((int) $id)) !== null) {
            return $product;
        }
        throw new \DomainException('Товар не найден');
    }

    /**
     * @param integer $qty
     * @param integer $maxQty
     * @return integer
     * @throws \DomainException if the product cannot be found
     */
    private function getQuantity($qty, $maxQty)
    {
        $quantity = (int) $qty > 0 ? (int) $qty : 1;
        if ($quantity > $maxQty) {
            throw new \DomainException('Товара в наличии всего ' . Html::encode($maxQty) . ' шт.');
        }
        return $quantity;
    }

    public function redirect($url, $statusCode = 302)

    {

        \Yii::$app->session->close();

        return parent::redirect($url, $statusCode);
    }
}
