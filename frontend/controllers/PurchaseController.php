<?php 
namespace frontend\controllers;

use Yii;
use common\models\Order;
use yii\web\Controller;

class PurchaseController extends Controller {
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        $orders = Order::find()
        ->where(['email' => Yii::$app->user->identity->email])
        ->all();
        if(empty($orders)) {
            Yii::$app->session->setFlash("warning", "No tienes compras realizadas");
            return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(['']));
        }
        return $this->render('index', ['orders' => $orders]);
    }

}