<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Product;
use yii\data\Pagination;
use yii\db\Query;

/**
 * Site controller
 */
class ShopController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchedParams = array();

        $query = Product::find()->where(['in_sell' => true]);
        $countQuery = clone $query;
        $totalProductsCount = $countQuery->count();
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 6]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $queryParams = Yii::$app->request->get();
        if (Yii::$app->request->get() && isset($queryParams['search_marketplace'])) {

            $query = Product::find()
                ->orderBy('id desc');

            //This searchedParams is for save the filter data from input client
            //sends it to view in array, if empty true, declare a new empty array
            $searchedParams = [
                'product_name' => $queryParams['product_name'],
            ];

            if (!empty($queryParams['product_name'])) {
                $query->orWhere(['ilike', 'product.seo_slug', strtolower($queryParams['product_name'])]);
            }
            $query->andWhere(['draft' => false]);
            $countQuery = clone $query;
            $totalProductsCount = $countQuery->count();
            $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 6]);
            $products = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        }
        return $this->render('index', [
            'products' => $products,
            'pages' => $pages,
            'searchedParams' => $searchedParams,
            'totalProductsCount' => $totalProductsCount,
        ]);
    }

    public function actionProductDetail($id, $slug)
    {
        $product = (new Query())
            ->select('*')
            ->from('product')
            ->where(['id' => $id])
            ->andWhere(['seo_slug' => $slug])
            ->one();
        $product['photos_url'] = Yii::$app->xpectrum->formatMediaUrl($product['photos_url']);
        $detail = Yii::$app->xpectrum->getProductInformationCart($id);

        return $this->renderAjax('product-detail', ['product' => $product]);
    }

    public function actionAddProduct()
    {
        $cart = Yii::$app->cart;
        $product = Product::find()->where(['id' => 12])->one();
        $cart->add($product, 12);
        if ($dataPost = Yii::$app->request->post()) {
            $product = Product::find()->where(['id' => $dataPost['product-id']])->one();
            $cart->add($product, $dataPost['quantity']);
            Yii::$app->session->setFlash('success', 'Se ha agregado al carrito.');
        } else {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error, revisa.');
        }

        //return $this->redirect(Yii::$app->urlManager->createAbsoluteUrl(['tienda']));
    }

}
