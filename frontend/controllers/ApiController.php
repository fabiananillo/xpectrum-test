<?php

namespace frontend\controllers;

use Yii;

use yii\web\Controller;

use common\models\Product;
use yii\db\Query;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\rest\Serializer;

/**
 * Api controller
 */
class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionProductList()
    {
        $productQuery = (new Query())
            ->select('id, product_name, photos_url, price')
            ->addSelect(['CONCAT(\'/api/product-detail?id=\',product.id) AS url_product_detail'])
            ->from('product');

        $pagination = new Pagination(['totalCount' => $productQuery->count(), 'pageSize' => 8]);
        $dataProvider = new ActiveDataProvider([
            'query' => $productQuery,
        ]);

        $dataProvider->setPagination($pagination);
        $serializer = new Serializer(['collectionEnvelope' => 'items']);
        $product = $serializer->serialize($dataProvider);
        foreach ($product['items'] as $key => $value) {
            $product['items'][$key]['photos_url'] = explode(",", str_replace(array('{', '}'), '', $value['photos_url']));
        }
        return \Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'statusCode' => 200,
            'data' => [
                'message' => 'lista de productos',
                'code' => 200,
                'result' => $product
            ],
        ]);
    }

    public function actionProductDetail($id)
    {

        $product = Product::find()->where(['id' => $id])->one();

        if (!empty($product)) {
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'statusCode' => 200,
                'data' => [
                    'message' => 'Detalle del producto',
                    'code' => 200,
                    'result' => $product
                ],
            ]);
        } else {
            return \Yii::createObject([
                'class' => 'yii\web\Response',
                'format' => \yii\web\Response::FORMAT_JSON,
                'statusCode' => 403,
                'data' => [
                    'message' => 'Producto no encontrado',
                    'code' => 403,
                ],
            ]);
         }
    }
}
