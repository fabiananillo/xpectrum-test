<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $first_name;
    public $last_name;
    public $country_id;
    public $state_id;
    public $city;
    public $apartment;
    public $zip_code;
    public $phone_code;
    public $phone_number;
    public $email;
    public $password;
    public $address;
    public $reCaptcha;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['first_name', 'last_name', 'address', 'phone_number', 'city', 'country_id', 'state_id', 'zip_code'], 'required', 'message' => 'Este campo no puede estar vacío.'],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Este correo ha sido utilizado.'],
            ['apartment', 'string'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            //['reCaptcha', 'required', 'message' => 'Termine de realizar la verificación.'],
            //['reCaptcha', \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6Lfw3FcUAAAAAJyeJEFRKeNuQqkWLreXOwvy6H03'],
        ];
    }

    public function attributeLabels() {
        return [
            'first_name' => 'Primer Nombre',
            'last_name' => 'Segundo Nombre',
            'address' => 'Dirección',
            'phone_number' => 'Teléfono',
            'email' => 'Correo Electrónico',
            'password' => 'Contraseña'
        ];
    }
    
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        
        $user = new User();
        $user->username = $this->email;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->phone_code = '+57';
        $user->phone_number = $this->phone_number;
        $user->country_id = $this->country_id;
        $user->state_id = $this->state_id;
        $user->city = $this->city;
        $user->apartment = $this->apartment;
        $user->zip_code = $this->zip_code;
        $user->address = $this->address;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->validate();
        if(empty($user->getErrors())) {
            $user->save();
            return $user;
        } else {
            return $user->getErrors();
        }
    }
}
