<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject = 'Contacto';
    public $body;
    public $reCaptcha;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required', 'message' => 'Este campo no puede estar vacío.'],
            // email has to be a valid email address
            ['email', 'email'],
            //['reCaptcha', 'required', 'message' => 'Termine de realizar la verificación.'],
            //['reCaptcha', \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6Lfw3FcUAAAAAJyeJEFRKeNuQqkWLreXOwvy6H03'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
            'body' => 'Mensaje',
            'name' => 'Nombre Completo',
            'email' => 'Correo Electrónico',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo('fabiananillo@gmail.com')
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject .' - ' .$this->name)
            ->setTextBody($this->body)
            ->send();
    }
}
