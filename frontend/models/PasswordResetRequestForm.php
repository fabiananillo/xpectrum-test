<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $reCaptcha;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //['reCaptcha', 'required', 'message' => 'Termine de realizar la verificación.'],
            //['reCaptcha', \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6Lfw3FcUAAAAAJyeJEFRKeNuQqkWLreXOwvy6H03'],
            ['email', 'trim'],
            ['email', 'required', 'message' => 'Este campo no puede estar vacío.'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'No existe usuario con este correo'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
                ->mailer
                ->compose(
                        ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => 'Vitral Mar'])
                ->setTo($user->email)
                ->setSubject('Restaurar contraseña para ' . Yii::$app->name)
                ->send();
    }
}
