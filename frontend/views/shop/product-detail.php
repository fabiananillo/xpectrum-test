<?php

use yii\helpers\Html;
use edofre\sliderpro\models\Slide;
use edofre\sliderpro\models\slides\Caption;
use edofre\sliderpro\models\slides\Image;
use edofre\sliderpro\models\Thumbnail;
use edofre\sliderpro\models\slides\Layer;

$slides = array();
$thumbnails = array();
foreach ($product['photos_url'] as $key => $value) {
    array_push($slides, new Slide([
        'items' => [
            new Image(['src' => $value])
        ]
    ]));
    array_push($thumbnails, new Thumbnail([
        'tag' => 'img',
        'htmlOptions' => [
            'src' =>  $value,
            'data-src' => $value
        ]
    ]));
}

?>

<div class="row">
    <div class="col-md-6">
        <?= \edofre\sliderpro\SliderPro::widget([
            'id'            => 'my-slider',
            'slides'        => $slides,
            'thumbnails'    => $thumbnails,
            'sliderOptions' => [
                'width'  => 600,
                'height' => 400,
                'arrows' => true,
                'init'   => new \yii\web\JsExpression("
                            function() {
                                console.log('slider is initialized');
                            }
                        "),
            ],
        ]);
        ?>
    </div>
    <div class="col-md-6">
        <h2 class="text-black"><?= $product['product_name']; ?></h2>
        <span>Descripción:</span>
        <p style="color:gray;"> <?= $product['description']; ?></p>
        <span>Precio:</span>
        <p><strong class="text-primary h4"><?= '$' . number_format($product['price'], 0); ?></strong></p>
        <?php if (empty(Yii::$app->xpectrum->getProductInformationCart($product['id']))) : ?>
            <?= Html::beginForm(Yii::$app->urlManager->createAbsoluteUrl(['carro/agregar'], 'https'), 'post'); ?>
            <div class="mb-5">
                <span>Cantidad:</span>
                <div class="input-group mb-3" style="max-width: 120px;">
                    <div class="input-group-prepend">
                        <?=

                                Html::button('-', [
                                    'id' => 'minus-btn',
                                    'class' => 'btn btn-outline-primary js-btn-plus',
                                    'onclick' => '
                                    var quantity = $("#quantity").val();
                                    if(quantity > 1){
                                        var newQuantity = (quantity - 1);
                                        $("#quantity").val(newQuantity);
                                    }
                                    '
                                ]);
                            ?>
                    </div>
                    <?= Html::textInput('quantity', 1, [
                        'class' => 'form-control text-center',
                         'id' => 'quantity',
                         'maxlength' => 2,
                         ]) ?>
                    <div class="input-group-append">
                        <?=

                                Html::button('+', [
                                    'id' => 'plus-btn',
                                    'class' => 'btn btn-outline-primary js-btn-plus',
                                    'onclick' => '
                                    var quantity = $("#quantity").val();
                                    if(quantity < 100){
                                        var newQuantity = (+quantity + +1);
                                        $("#quantity").val(newQuantity);
                                    }'
                                ]);
                            ?>
                    </div>
                </div>
            </div>
            <?= Html::hiddenInput('product-id', $product['id']); ?>
            <?= Html::submitButton('Agregar Al Carrito', ['class' => 'btn-new-btn btn-test']) ?>
            <?= Html::endForm(); ?>
        <?php else : ?>
            <div class="mb-5">
                <div class="input-group mb-3" style="max-width: 120px;">
                    <span>Este producto ya está en tu carro de compras</span>
                    <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['carro'], 'https') ?>" class="btn-new-btn btn-test">Ir al carro</a>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>