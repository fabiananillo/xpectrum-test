<?php

use common\models\ProductCategory;
use common\models\ProductSubcategory;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;
?>
<div class="bg-light py-3" style="padding-top: 140px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Tienda</strong></div>
        </div>
    </div>
</div>
<?php
Pjax::begin([
    'enablePushState' => false,
    'timeout' => 300000,
]);
?>
<div class="site-section">
    <div class="container">

        <div class="row mb-5">
            <div class="col-md-9 order-2">

                <div class="row">
                    <div class="col-md-12 mb-5">
                        <div class="float-md-left mb-4">
                            <h2 class="text-black h5"><?= $totalProductsCount; ?> Resultados encontrados.</h2>
                        </div>
                       
                    </div>
                </div>
                <div class="row mb-5">
                    <?php foreach ($products as $key => $value) : ?>
                        <div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
                            <div class="block-4 text-center border">
                                <figure class="block-4-image">
                                    <img src="<?= $value->photos_url[0]; ?>" alt="Image placeholder" class="img-fluid" style="height:250px; width:429px;">
                                </figure>
                                <div class="block-4-text p-4">
                                    <h3><?= $value->product_name ?></h3>
                                    <p class="mb-0" style="color:gray;"><?= $value->description ?></p>
                                    <p class="text-primary font-weight-bold"><?= '$' . number_format($value->price, 0); ?></p>
                                    <?=
                                            Html::button('Ver Producto', [
                                                'value' => Yii::$app->urlManager->createAbsoluteUrl(['detalle-producto', 'id' => $value->id, 'slug' => $value->seo_slug], 'https'),
                                                'class' => 'btn-new-btn btn-test',
                                                'data-toggle' => 'modal',
                                                'data-target' => '.bd-example-modal-lg',
                                                'onclick' => '$("#product-content")
                                                        .load($(this).attr("value"));'
                                            ]);
                                        ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="site-block-27">
                            <?= LinkPager::widget([
                                'pagination' => $pages,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 order-1 mb-5 mb-md-0">
                <!-- search -->
                <?= Html::beginForm('tienda', 'get', [
                    'enctype' => 'multipart/form-data',
                    'data-pjax' => ''
                ]);
                ?>
                <?= Html::hiddenInput('search_marketplace', true) ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= Html::textInput('product_name', empty($searchedParams['product_name']) ? null : $searchedParams['product_name'], ['placeholder' => 'Ingresa tu búsqueda aquí...']) ?>
                        <br><br>
                    </div>

                    <div class="col-md-12">
                        <?= Html::submitButton('Buscar', ['class' => 'btn-new-btn btn-test']) ?>
                    </div>
                </div>
                <?= Html::endForm(); ?>
            </div>

        </div>
    </div>
</div>
<?php
Pjax::end();
?>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detalles del Producto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="product-content">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-new-btn btn-test" data-dismiss="modal">Cerrar X</button>
            </div>
        </div>
    </div>
</div>