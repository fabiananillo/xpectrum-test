<?php
use common\models\User;
?>
<div class="bg-light py-3" style="padding-top: 140px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a> <span class="mx-2 mb-0">/</span> <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['carro']) ?>">Carro</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Generar Orden</strong></div>
        </div>
    </div>
</div>
<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-5 mb-md-0">
                <h2 class="h3 mb-3 text-black">Resumen</h2>
                <div class="p-3 p-lg-5 border">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Nombre: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= $dataPost['first_name'];?></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Apellido: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= $dataPost['last_name'];?></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Correo Electrónico: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= $dataPost['email'];?></label>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">País: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= Yii::$app->xpectrum->getCountryNameById($dataPost['country_id']);?></label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Departamento: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= Yii::$app->xpectrum->getStateNameById($dataPost['state_id']);?></label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Ciudad: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= $dataPost['city'];?></label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Dirección: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= $dataPost['address'];?></label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Apartamento / Casa / Suite: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= $dataPost['apartment'];?></label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Código Postal: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= $dataPost['zip_code'];?></label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Teléfono: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= $dataPost['phone'];?></label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="text-black">Notas Adicionales: </label>
                        </div>
                        <div class="col-md-6">
                            <label><?= $dataPost['order_notes'];?></label>
                        </div>
                    </div>
                    

                </div>
            </div>
            <div class="col-md-6">
                <?php if (!empty($cartItems = $cart->getItems())) : ?>
                    <div class="row mb-5">
                        <div class="col-md-12">
                            <h2 class="h3 mb-3 text-black">Tu Orden</h2>
                            <div class="p-3 p-lg-5 border">
                                <table class="table site-block-order-table mb-5">
                                    <thead>
                                        <th>Producto</th>
                                        <th>Total</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($cartItems as $item) : ?>
                                            <?php $product = $item->getProduct(); ?>
                                            <tr>
                                                <td><?= $product->product_name; ?> <strong class="mx-2">x</strong> <?= $item->getQuantity() ?></td>
                                                <td><?= '$' . number_format($item->getCost(), 0) ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td class="text-black font-weight-bold"><strong>Subtotal</strong></td>
                                            <td class="text-black"><?= '$' . number_format($cart->getTotalCost()); ?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-black font-weight-bold"><strong>Total a Pagar</strong></td>
                                            <td class="text-black font-weight-bold"><strong><?= '$' . number_format($cart->getTotalCost()); ?></strong></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <form>
                                        <script src='https://checkout.epayco.co/checkout.js' 
                                        data-epayco-key='ca8c9e57a85785f06f098ace78b57a6e' 
                                        class='epayco-button' 
                                        data-epayco-email-billing="<?= $dataPost['email'];?>"
                                        data-epayco-name-billing="<?= $dataPost['first_name']. ' ' . $dataPost['last_name']; ?>"
                                        data-epayco-address-billing="<?= $dataPost['address'];?>"
                                        data-epayco-mobilephone-billing="<?= $dataPost['phone'] ?>"
                                        data-epayco-extra1='<?= $orderProductPaymentId ?>'
                                        data-epayco-extra3='<?= Yii::$app->user->isGuest ? User::IS_GUEST : Yii::$app->user->identity->id;?>'
                                        data-epayco-extra4='<?= $dataPost['country_id'] ?>'
                                        data-epayco-extra5='<?= $dataPost['state_id'] ?>'
                                        data-epayco-extra6='<?= $dataPost['city'] ?>'
                                        data-epayco-extra7='<?= $dataPost['zip_code'] ?>'
                                        data-epayco-extra8='<?= $dataPost['phone'] ?>'
                                        data-epayco-extra9='<?= $dataPost['order_notes'] ?>'
                                        data-epayco-extra10='<?= $dataPost['apartment'] ?>'
                                        data-epayco-amount='<?= $cart->getTotalCost(); ?>' 
                                        data-epayco-tax='0' 
                                        data-epayco-tax-base='0'
                                        data-epayco-name='Xpectrum Test'
                                        data-epayco-description='Xpectrum Test'
                                        data-epayco-currency='COP' 
                                        data-epayco-country='CO'
                                        data-epayco-test='true'
                                        data-epayco-external='false' 
                                        data-epayco-response='https://xpectrum-test.herokuapp.com/orden/transaccion' 
                                        data-epayco-confirmation='https://xpectrum-test.herokuapp.com/orden/confirmacion' 
                                        data-epayco-button='https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/boton_carro_de_compras_epayco2.png'>
                                        </script>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

    </div>
</div>