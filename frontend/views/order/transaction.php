<div class="bg-light py-3" style="padding-top: 140px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Transacción</strong></div>
        </div>
    </div>
</div>

<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <span class="icon-check_circle display-3 text-success"></span>
                <h2 class="display-3 text-black">¡Gracias!</h2>
                <p class="lead mb-5">Hemos recibido tu orden <?= $dataPayco['x_customer_name'] . ' ' . $dataPayco['x_customer_lastname']; ?>
                    <br>
                    Si no haz completado el proceso de compra, tienes un plazo de 24hrs. para hacerlo.
                    <br>
                    A continuación te mostramos el estado del proceso: 
                    <br>
                    <b><?= $dataPayco['x_respuesta']; ?></b>
                </p>
                <p><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['tienda']) ?>" class="btn-new-btn btn-test">Regresar a la tienda</a></p>
            </div>
        </div>
    </div>
</div>