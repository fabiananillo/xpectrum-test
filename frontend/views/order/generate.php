<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Generar Orden';
?>
<div class="bg-light py-3" style="padding-top: 140px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a> <span class="mx-2 mb-0">/</span> <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['carro']) ?>">Carro</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Generar Orden</strong></div>
        </div>
    </div>
</div>

<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-5 mb-md-0">
                <h2 class="h3 mb-3 text-black">Detalles Facturación</h2>
                <?php
                $form = ActiveForm::begin([
                    'id' => 'create-order-form',
                    'action' => 'pago',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ]);
                ?>
                <div class="p-3 p-lg-5 border">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'first_name')->textInput([
                                'autofocus' => true,
                                'id' => 'first_name',
                                'class' => 'form-control',
                                'value' => !(Yii::$app->user->isGuest) ? Yii::$app->user->identity->first_name : null,
                            ])->label('Nombre');
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'last_name')->textInput([
                                'class' => 'form-control',
                                'id' => 'last_name',
                                'value' => !(Yii::$app->user->isGuest) ? Yii::$app->user->identity->last_name : null,
                            ])->label('Apellido');
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'email')->textInput([
                                'autofocus' => true,
                                'id' => 'email',
                                'class' => 'form-control',
                                'value' => !(Yii::$app->user->isGuest) ? Yii::$app->user->identity->username : null,
                            ])->label('Correo Electrónico');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?=
                            $form->field($model, 'country_id')->dropDownList($countries, [
                                'onchange' => '$.post( "' . Yii::$app->urlManager->createAbsoluteUrl(['estado?id=']) . '"+$(this).val(),function( data ){ 
                                    $( "select#order-state-form" ).html( data );
                                });',
                                'class' => 'form-control',
                                'id' => 'country_id',
                                'value' => 47

                            ])->label('País');
                        ?>
                        <?=
                            $form->field($model, 'state_id')->dropDownList($defaultStates, [
                                'id' => 'order-state-form',
                                'prompt' => 'Primero seleccione país',
                                'id' => 'state_id',
                                'value' => 778,
                            ])->label('Departamento o Estado')
                        ?>
                        <?= $form->field($model, 'city')->textInput([
                            'class' => 'form-control',
                            'id' => 'city',
                        ])->label('Ciudad o Municipio');
                        ?>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'address')->textInput([
                                'class' => 'form-control',
                                'id' => 'address',
                                'value' => !(Yii::$app->user->isGuest) ? Yii::$app->user->identity->address : null,
                            ])->label('Dirección');
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'apartment')->textInput([
                            'class' => 'form-control',
                            'id' => 'apartment',
                            'placeholder' => 'Apartamento, casa, suite, etc. (Opcional)'
                        ])->label(false);
                        ?>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'zip_code')->textInput([
                                'id' => 'zip_code',
                                'class' => 'form-control',
                            ])->label('Código Postal');
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'phone')->textInput([
                                'id' => 'phone',
                                'class' => 'form-control',
                                'value' => !(Yii::$app->user->isGuest) ? Yii::$app->user->identity->phone_number : null,
                            ])->label('Teléfono');
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'order_notes')->textArea([
                            'class' => 'form-control',
                            'id' => 'order_notes',
                            'rows' => 6,
                            'style' => 'resize:none;'
                        ])->label('Notas Adicionales');
                        ?>

                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Ir a Pagar', ['class' => 'btn-new-btn btn-test']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div> 
        <!-- </form> -->
    </div>
</div>