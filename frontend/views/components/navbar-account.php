<?php 
use yii\helpers\Html;
?>
<div class="cur-lang-acc-active">
    <div class="wrap-sidebar">
        <div class="sidebar-nav-icon">
            <button class="op-sidebar-close"><span class="ion-android-close"></span></button>
        </div>
        <div class="cur-lang-acc-all">
            <div class="single-currency-language-account">
                <?php if (!Yii::$app->user->isGuest): ?>
                    <div class="cur-lang-acc-title">
                        <h4>Hola <?= Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name; ?> </h4>
                    </div>
                    <div class="cur-lang-acc-dropdown">

                        <ul>
                            <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['mi-cuenta']); ?>">Acceder a mi cuenta</a></li>
                        </ul>
                        <br>
                        <?= Html::beginForm('salir');?>
                            <?= Html::submitButton('Cerrar Sesión', ['class' => 'btn btn-primary'])?>
                        <?= Html::endForm();?>

                    </div>
                <?php else: ?>
                    <div class="cur-lang-acc-title">
                        <h4>Cuenta: </h4>
                    </div>
                    <div class="cur-lang-acc-dropdown">

                        <ul>
                            <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['registro']); ?>">Registro</a></li>
                            <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['ingreso']); ?>">Ingreso</a></li>
                        </ul>

                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>