<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Carro';
$counter = 0;
?>
<div class="bg-light py-3" style="padding-top: 140px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Carro</strong></div>
        </div>
    </div>
</div>
<?php
Pjax::begin([
    'enablePushState' => false,
    'timeout' => 300000,
]);
?>
<div class="site-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <h2 class="h3 mb-3 text-black">Tu carro de compras</h2>
            </div>
            <?php if (!empty($cartItems = $cart->getItems())) : ?>
                <?= Html::beginForm('carro', 'post', [
                        'enctype' => 'multipart/form-data',
                        'data-pjax' => '',
                        'class' => 'col-md-12'
                    ]);
                    ?>
                <form class="col-md-12" method="post">
                    <div class="site-blocks-table">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="product-thumbnail">Imagen</th>
                                    <th class="product-name">Producto</th>
                                    <th class="product-price">Precio</th>
                                    <th class="product-quantity">Cantidad</th>
                                    <th class="product-total">Total</th>
                                    <th class="product-remove">Remover</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($cartItems as $item) : ?>
                                    <?php $product = $item->getProduct(); ?>
                                    <?php $counter += 1; ?>
                                    <tr>
                                        <td class="product-thumbnail">
                                            <img src="<?= $product->photos_url[0]; ?>" alt="Image" class="img-fluid" style="height:150px; width:429px;">
                                        </td>
                                        <td class="product-name">
                                            <h2 class="h5 text-black"><?= $product->product_name; ?></h2>
                                        </td>
                                        <td><?= '$' . number_format($product->price, 0); ?></td>
                                        <td>
                                            <div class="input-group mb-3" style="max-width: 120px;">
                                                <div class="input-group-prepend">
                                                    <button id="<?= $counter ?>" name="minus" class="btn btn-outline-primary js-btn-minus" type="button">&minus;</button>
                                                </div>
                                                <input min="1" id="quantity<?= $counter ?>" name="quantity-input" type="text" class="form-control text-center" value="<?= $item->getQuantity() ?>" data-input="<?= $counter; ?>" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                                                <div class="input-group-append">
                                                    <button id="<?= $counter ?>" name="plus" class="btn btn-outline-primary js-btn-plus" type="button">&plus;</button>
                                                </div>
                                            </div>
                                            <?= Html::hiddenInput('url' . $counter, Yii::$app->urlManager->createAbsoluteUrl(['carro/cambiar?productId=' . $product->id . '&quantity=']), ['id' => 'url' . $counter]) ?>
                                        </td>
                                        <td><?= '$' . number_format($item->getCost(), 0) ?></td>
                                        <td><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['carro/remover?id=' . $product->id]) ?>" class="btn btn-primary btn-sm">X</a></td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                    <?= Html::endForm(); ?>

                <?php else : ?>
                    <span>No hay nada en tu carro de compras aún.</span>
                <?php endif; ?>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="row mb-5">
                    <div class="col-md-6 mb-3 mb-md-0">
                        <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['carro/limpiar']) ?>" class="btn-new-btn btn-test">Limpiar Carro</a>
                    </div>
                    <div class="col-md-6">
                        <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['tienda']) ?>" class="btn-new-btn btn-test">Continuar Comprando</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-5">
                <div class="row justify-content-end">
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-12 text-right border-bottom mb-5">
                                <h3 class="text-black h4 text-uppercase">Total Compra</h3>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <span class="text-black">Subtotal</span>
                            </div>
                            <div class="col-md-6 text-right">
                                <strong class="text-black"><?= '$' . number_format($cart->getTotalCost()); ?></strong>
                            </div>
                        </div>
                        <div class="row mb-5">
                            <div class="col-md-6">
                                <span class="text-black">Total</span>
                            </div>
                            <div class="col-md-6 text-right">
                                <strong class="text-black"><?= '$' . number_format($cart->getTotalCost()); ?></strong>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn-new-btn btn-test" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['orden/generar']) ?>">Ir a Pagar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
Pjax::end();
?>