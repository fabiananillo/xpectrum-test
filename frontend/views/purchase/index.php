<div class="bg-light py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="index.html">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Compras</strong></div>
        </div>
    </div>
</div>

<div class="site-section">
    <div class="container">
        <div class="row mb-5">
            <form class="col-md-12" method="post">
                <div class="site-blocks-table">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="product-thumbnail">Método de Pago</th>
                                <th class="product-thumbnail">Estado</th>
                                <th class="product-thumbnail">Factura</th>
                                <th class="product-name">Productos</th>
                                <th class="product-price">Total</th>
                                <th class="product-quantity">Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($orders as $key => $value) : ?>
                                <?php $orderProductDetail = Yii::$app->xpectrum->getOrderProductDetail($value->id); ?>
                                <?php $payment = Yii::$app->xpectrum->getPaymentInformation($value->payment_id); ?>

                                <tr>
                                    <td class="product-name">
                                        <span><?= $payment->transfer_method; ?></span>
                                    </td>
                                    <td class="product-name">
                                        <span><?= $payment->status; ?></span>
                                    </td>
                                    <td class="product-thumbnail">
                                        <a href="<?= Yii::getAlias('@web') . '/temp_files/invoices/' . $value->invoice; ?>" download>Descargar </a>
                                    </td>
                                    <td class="product-name">
                                        <!-- product info -->
                                        <?php foreach ($orderProductDetail as $keyProduct => $valueProduct) : ?>
                                            <span><?= Yii::$app->xpectrum->getProductInformationById($valueProduct->product_id)->product_name . ' x ' . $valueProduct->quantity . ' ( ' . $valueProduct->total_price . ' )'; ?></span>
                                            <br>
                                        <?php endforeach; ?>
                                    </td>
                                    <td><?= $value->total_payment; ?></td>
                                    <td class="product-name">
                                        <span><?= date_format(date_create($value->created_at), 'Y-m-d H:i a'); ?></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>