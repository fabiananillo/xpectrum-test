<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Registro';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bg-light py-3" style="padding-top: 140px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Registro</strong></div>
        </div>
    </div>
</div>
<!-- register-area start -->
<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="h3 mb-3 text-black">Registra tu cuenta completando los siguientes campos</h2>
            </div>
            <div class="col-md-8">

                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <div class="p-3 p-lg-5 border">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'first_name')->textInput([
                                'autofocus' => true,
                                'class' => 'form-control'
                            ])->label('Primer Nombre');
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'last_name')->textInput([
                                'class' => 'form-control'
                            ])->label('Segundo Nombre');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?=
                            $form->field($model, 'country_id')->dropDownList($countries, [
                                'onchange' => '$.post( "' . Yii::$app->urlManager->createAbsoluteUrl(['estado?id=']) . '"+$(this).val(),function( data ){ 
                                    $( "select#order-state-form" ).html( data );
                                });',
                                'class' => 'form-control',
                                'id' => 'country_id',
                                'value' => 47

                            ])->label('País');
                        ?>
                        <?=
                            $form->field($model, 'state_id')->dropDownList($defaultStates, [
                                'id' => 'order-state-form',
                                'prompt' => 'Primero seleccione país',
                                'id' => 'state_id',
                                'value' => 778,
                            ])->label('Departamento o Estado')
                        ?>
                        <?= $form->field($model, 'city')->textInput([
                            'class' => 'form-control',
                            'id' => 'city',
                        ])->label('Ciudad o Municipio');
                        ?>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'address')->textInput([
                                'class' => 'form-control',
                                'id' => 'address',
                                'value' => !(Yii::$app->user->isGuest) ? Yii::$app->user->identity->address : null,
                            ])->label('Dirección');
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'apartment')->textInput([
                            'class' => 'form-control',
                            'id' => 'apartment',
                            'placeholder' => 'Apartamento, casa, suite, etc. (Opcional)'
                        ])->label(false);
                        ?>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'zip_code')->textInput([
                                'id' => 'zip_code',
                                'class' => 'form-control',
                            ])->label('Código Postal');
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'phone_number')->textInput([
                                'class' => 'form-control'
                            ])->label('Teléfono'); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'email')->textInput([
                                'class' => 'form-control'
                            ])->label('Correo Electrónico'); ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'password')->passwordInput([
                                'class' => 'form-control'
                            ])->label('Contraseña'); ?>
                            <?=
                                $form->field($model, 'reCaptcha')->widget(
                                    \himiklab\yii2\recaptcha\ReCaptcha::className(),
                                    ['siteKey' => '6Lfw3FcUAAAAAEv8DIA3wch5q5TaDtZo31AFmmvT']
                                )->label(false)
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-md-12">
                            <input type="submit" class="btn btn-primary btn-lg btn-block" value="Crear Cuenta">
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<!-- register-area end -->