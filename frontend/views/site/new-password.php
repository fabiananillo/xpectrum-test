<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Clave Nueva';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bg-light py-3" style="padding-top: 140px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Ingreso</strong></div>
        </div>
    </div>
</div>
<!-- login start -->
<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="h3 mb-3 text-black">Por favor, escribe tu nueva contraseña:</h2>
            </div>
            <div class="col-md-8">

                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <div class="p-3 p-lg-5 border">
                    <div class="form-group row">
                        <div class="col-md-12">
                        <?= $form->field($model, 'password')->passwordInput([
                            'autofocus' => true,
                            ])->label('Nueva Contraseña'); ?>
                        </div>
                        <div class="col-md-12">
                        <?= $form->field($model, 'repeat_password')->passwordInput()->label('Repetir Contraseña') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-md-12">
                            <input type="submit" class="btn btn-primary btn-lg btn-block" value="Guardar Cambios">
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<!-- login end -->

