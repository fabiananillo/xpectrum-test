<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Error';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bg-light py-3" style="padding-top: 140px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Error</strong></div>
        </div>
    </div>
</div>

<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="site-error">

                <h1><?= Html::encode($this->title) ?></h1>

                <div class="alert alert-danger">
                    <?= nl2br(Html::encode($message)) ?>
                </div>

                <p>
                   Algo ha salido mal. 
                </p>
                <p>
                   Ponte en contacto con un administrador.
                </p>

            </div>

        </div>
    </div>
</div>