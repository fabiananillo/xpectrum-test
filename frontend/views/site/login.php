<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Ingreso';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bg-light py-3" style="padding-top: 140px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Ingreso</strong></div>
        </div>
    </div>
</div>
<!-- login start -->
<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="h3 mb-3 text-black">Inicia sesión</h2>
            </div>
            <div class="col-md-8">

                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <div class="p-3 p-lg-5 border">
                    <div class="form-group row">
                        <div class="col-md-12">

                            <?= $form->field($model, 'username')->textInput([
                                'autofocus' => true,
                                'class' => 'form-control'
                            ])->label('Correo Electrónico'); ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'password')->passwordInput([
                                'class' => 'form-control'
                            ])->label('Contraseña'); ?>
                        </div>
                        <span>¿No tienes una cuenta? <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['registro'])?>">Crear Cuenta</a></span>
                        <span>¿No recuerdas tu contraseña? <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['recuperar-contraseña'])?>">Recuperar Contraseña</a></span>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <?=
                                $form->field($model, 'reCaptcha')->widget(
                                    \himiklab\yii2\recaptcha\ReCaptcha::className(),
                                    ['siteKey' => '6Lfw3FcUAAAAAEv8DIA3wch5q5TaDtZo31AFmmvT']
                                )->label(false)
                            ?>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <input type="submit" class="btn-new-btn btn-test" value="Ingresar">
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<!-- login end -->