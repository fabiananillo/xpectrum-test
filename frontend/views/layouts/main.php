<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$url = Yii::$app->request->url;
echo yii2mod\alert\Alert::widget();
?>
<?php

$currentUrl = Yii::$app->request->hostInfo . Yii::$app->request->url;


if (!strpos($currentUrl, "localhost")) {

    if (!(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' ||
        $_SERVER['HTTPS'] == 1) ||
        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
        $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' . $redirect);
        exit();
    }
} 

?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <title><?= Html::encode($this->title); ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/fonts/icomoon/style.css">

    <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/css/jquery-ui.css">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/css/owl.theme.default.min.css">
    <script src="https://kit.fontawesome.com/78fe7cdcec.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/css/aos.css">

    <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/css/style.css">
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
    <div class="site-wrap">
         <!-- Navigation -->
         <nav class="navbar navbar-expand-lg navbar-light py-3" id="mainNav" style="background-color:#34e079;">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="<?= Yii::getAlias('@web') ?>/images/logo.png" style="height: 50%; width: 50%;"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars" style="color:white;"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link js-scroll-trigger" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['']) ?>">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['tienda']) ?>">Tienda</a>
                        </li>
                        <?php if (Yii::$app->user->isGuest) : ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Acceder
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['ingreso']) ?>">Iniciar Sesión</a>
                                    <a class="dropdown-item" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['registro']) ?>">Crear Cuenta</a>
                                </div>
                            </li>
                        <?php else : ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Hola <?= Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name; ?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                
                                    <?= Html::beginForm(Yii::$app->urlManager->createAbsoluteUrl(['salir'], 'https'), 'post') ?>

                                    <?= Html::submitButton('Salir', [
                                            'class' => 'nav-link js-scroll-trigger',
                                            'style' => 'padding-left: 22px;
                                                    border: none;
                                                    color:black;
                                                    background: none; cursor: pointer;'
                                        ]);
                                        ?>
                                    <?= Html::endForm() ?>
                                </div>
                            </li>
                        <?php endif; ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Yii::$app->urlManager->createAbsoluteUrl(['carro']) ?>"><i class="fas fa-shopping-cart"></i> <span class="badge badge-light"><?= Yii::$app->xpectrum->getCountProductsCart(); ?></span></a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
        </header>

        <?= $content; ?>

        <footer class="site-footer border-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 mb-5 mb-lg-0">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="footer-heading mb-4">Navigations</h3>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <ul class="list-unstyled">
                                    <li><a href="#">Sell online</a></li>
                                    <li><a href="#">Features</a></li>
                                    <li><a href="#">Shopping cart</a></li>
                                    <li><a href="#">Store builder</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <ul class="list-unstyled">
                                    <li><a href="#">Mobile commerce</a></li>
                                    <li><a href="#">Dropshipping</a></li>
                                    <li><a href="#">Website development</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <ul class="list-unstyled">
                                    <li><a href="#">Point of sale</a></li>
                                    <li><a href="#">Hardware</a></li>
                                    <li><a href="#">Software</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                        <h3 class="footer-heading mb-4">Promo</h3>
                        <a href="#" class="block-6">
                            <img src="images/hero_1.jpg" alt="Image placeholder" class="img-fluid rounded mb-4">
                            <h3 class="font-weight-light  mb-0">Finding Your Perfect Shoes</h3>
                            <p>Promo from nuary 15 &mdash; 25, 2019</p>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="block-5 mb-5">
                            <h3 class="footer-heading mb-4">Contact Info</h3>
                            <ul class="list-unstyled">
                                <li class="address">203 Fake St. Mountain View, San Francisco, California, USA</li>
                                <li class="phone"><a href="tel://23923929210">+2 392 3929 210</a></li>
                                <li class="email">emailaddress@domain.com</li>
                            </ul>
                        </div>

                        <div class="block-7">
                            <form action="#" method="post">
                                <label for="email_subscribe" class="footer-heading">Subscribe</label>
                                <div class="form-group">
                                    <input type="text" class="form-control py-4" id="email_subscribe" placeholder="Email">
                                    <input type="submit" class="btn btn-sm btn-primary" value="Send">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row pt-5 mt-5 text-center">
                    <div class="col-md-12">
                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
                            <script>
                                document.write(new Date().getFullYear());
                            </script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" class="text-primary">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>

                </div>
            </div>
        </footer>
    </div>

    <script src="<?= Yii::getAlias('@web'); ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/js/jquery-ui.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/js/popper.min.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/js/bootstrap.min.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/js/owl.carousel.min.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/js/aos.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/js/cart.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/js/main.js"></script>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>