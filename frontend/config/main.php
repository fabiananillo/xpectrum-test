<?php

$params = array_merge(
        require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

//obtener host para configurar prettyurl
use \yii\web\Request;

$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
$baseUrl2 = str_replace('/backend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'language' => 'es-ES',
    'name' => 'Xpectrum Test',
    'bootstrap' => ['log'],
    'timeZone' => 'America/Bogota',
    'defaultRoute' => 'site/index',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            //urlFormat => 'path',
            'baseUrl' => $baseUrl,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                //Aquí agregas las url amigables, atribuir la direccion de la vista
                'index' => 'site/index',
                'registro' => 'site/signup',
                'ingreso' => 'site/login',
                'recuperar-contraseña' => 'site/request-password-reset',
                'clave-nueva' => 'site/new-password',
                'salir' => 'site/logout',
                'estado' => 'site/state',
                'subcategory' => 'site/subcategory',
                'tienda' => 'shop/index',
                'detalle-producto' => 'shop/product-detail',
                'carro' => 'cart/index',
                'carro/agregar' => 'cart/add',
                'carro/limpiar' => 'cart/clear',
                'carro/remover' => 'cart/remove',
                'carro/cambiar' => 'cart/change',
                'orden/generar' => 'order/generate',
                'orden/pago' => 'order/payment',
                'orden/transaccion' => 'order/transaction',
                'orden/confirmacion' => 'order/confirmation',
                'compras' => 'purchase/index',

            ],
        ],
    ],
    'params' => $params,
];
