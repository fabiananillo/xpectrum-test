$(function () {

    $('.help-block').css('color', '#d14242');
    $('button[name=plus]').click(function (e) {
        var id = e.target.id;

        var quantity = $('#quantity' + id).val();
        var url = $('#url' + id).val() + quantity;
        $.ajax({
            url: url,
            type: 'get',
            context: document.body,
            statusCode: {
                302: function (data) {
                    if (window.console) {
                        window.location.href = window.location.origin + '/' + window.location.pathname.split('/')[1] + '/carro';
                    }
                },
                401: function (xhr) {
                    if (window.console)
                        console.log(xhr.responseText);
                }
            }
        });
    });
    $('button[name=minus]').click(function (e) {
        var id = e.target.id;

        var quantity = $('#quantity' + id).val();
        var url = $('#url' + id).val() + quantity;
        $.ajax({
            url: url,
            type: 'get',
            context: document.body,
            statusCode: {
                302: function (data) {
                    if (window.console) {
                        window.location.href = window.location.origin + '/' + window.location.pathname.split('/')[1] + '/carro';
                    }
                },
                401: function (xhr) {
                    if (window.console)
                        console.log(xhr.responseText);
                }
            }
        });
    });
    //called when key is pressed in textbox
    $("#quantity").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
    $('#quantity').keypress(function () {
        var maxLength = $(this).val().length;
        if (maxLength >= 2) {
            alert('No puedes ingresar más de ' + maxLength + ' cifras');
            return false;
        }
    });
    $('input[name=quantity-input]').keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
        var maxLength = $(this).val().length;
        if (maxLength >= 2) {
            alert('No puedes ingresar más de ' + maxLength + ' cifras');
            return false;
        }

    });
    $('input[name=quantity-input]').keyup(function (e) {
        var id = e.target.id;
        var getId = $('#' + id).attr('data-input');
        var quantity = $('#quantity' + getId).val();
        var url = $('#url' + getId).val() + quantity;

        $.ajax({
            url: url,
            type: 'get',
            context: document.body,
            statusCode: {
                302: function (data) {
                    if (window.console) {
                        window.location.href = window.location.origin + '/' + window.location.pathname.split('/')[1] + '/carro';
                    }
                },
                401: function (xhr) {
                    if (window.console)
                        console.log(xhr.responseText);
                }
            }
        });

    });
});