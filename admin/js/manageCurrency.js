$(function() {
    var autoNumericInstance = new AutoNumeric('#input-price', {
        currencySymbol: '$',
        decimalPlaces: 0,
        maximumValue: 10000000000000000
    });

    $('#input-price').on('keyup', function() {
        $('#output-price').val(autoNumericInstance.getNumericString());
    });

    function readURL(input, imgControlName) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(imgControlName).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#photo-1").change(function() {
        // add your logic to decide which image control you'll use
        var imgControlName = "#ImgPreview";
        $('#removeImage1').css('visibility', 'visible');
        readURL(this, imgControlName);
    });
    $("#photo-2").change(function() {
        // add your logic to decide which image control you'll use
        var imgControlName = "#ImgPreview2";
        $('#removeImage2').css('visibility', 'visible');
        readURL(this, imgControlName);
    });
    $("#photo-3").change(function() {
        // add your logic to decide which image control you'll use
        var imgControlName = "#ImgPreview3";
        $('#removeImage3').css('visibility', 'visible');
        readURL(this, imgControlName);
    });
    $("#photo-4").change(function() {
        // add your logic to decide which image control you'll use
        var imgControlName = "#ImgPreview4";
        $('#removeImage4').css('visibility', 'visible');
        readURL(this, imgControlName);
    });
    $("#removeImage1").click(function(e) {
        e.preventDefault();
        $("#photo-1").val("");
        $('#removeImage1').css('visibility', 'hidden');
        $("#ImgPreview").attr("src", "");
        $('.preview1').removeClass('it');
        $('.btn-rmv1').removeClass('rmv');
    });
    $("#removeImage2").click(function(e) {
        e.preventDefault();
        $("#photo-2").val("");
        $('#removeImage2').css('visibility', 'hidden');
        $("#ImgPreview2").attr("src", "");
        $('.preview2').removeClass('it');
        $('.btn-rmv2').removeClass('rmv');
    });
    $("#removeImage3").click(function(e) {
        e.preventDefault();
        $("#photo-3").val("");
        $('#removeImage3').css('visibility', 'hidden');
        $("#ImgPreview3").attr("src", "");
        $('.preview3').removeClass('it');
        $('.btn-rmv3').removeClass('rmv');
    });
    $("#removeImage4").click(function(e) {
        e.preventDefault();
        $("#photo-4").val("");
        $('#removeImage4').css('visibility', 'hidden');
        $("#ImgPreview4").attr("src", "");
        $('.preview4').removeClass('it');
        $('.btn-rmv4').removeClass('rmv');
    });

});