<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=matecardbinstance.ci1u1mvdowd7.us-east-1.rds.amazonaws.com;dbname=xpectrum_dev',
            'username' => 'matecar',
            'password' => 'Matecar2019!',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // Envio de correo por defecto
            // 'useFileTransport' falso para enviar correos
            // Configurar host, correo y puerto
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'fabiananillo@gmail.com',
                'password' => 'asifyvzakjwvxovd',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6Lfw3FcUAAAAAEv8DIA3wch5q5TaDtZo31AFmmvT',
            'secret' => '6Lfw3FcUAAAAAJyeJEFRKeNuQqkWLreXOwvy6H03',
        ],
        'xpectrum' => [
            'class' => 'common\components\xpectrum',
        ],
        'image' => array(
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD', //GD or Imagick
        ),
        'cart' => [
            'class' => 'devanych\cart\Cart',
            'storageClass' => 'devanych\cart\storage\SessionStorage',
            'calculatorClass' => 'devanych\cart\calculators\SimpleCalculator',
            'params' => [
                'key' => 'cart',
                'expire' => 36288000,
                'productClass' => 'common\models\Product',
                'productFieldId' => 'id',
                'productFieldPrice' => 'price',
            ],
        ],
        //Sentry App
        'sentry' => [
            'class' => 'mito\sentry\Component',
            'dsn' => 'https://39f713f220ca4aa3a1ad3f86995c55ef@sentry.io/1767523', // private DSN
            'environment' => 'development', // if not set, the default is `production`
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'mito\sentry\Target',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:404',
                    ],
                ],
            ],
        ],
    ],
];
