<?php
return [
    'adminEmail' => 'fabiananillo@gmail.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
