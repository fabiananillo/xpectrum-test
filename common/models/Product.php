<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\Expression;
use common\models\ProductCategory;
use common\models\ProductSubcategory;
/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $product_category_id
 * @property string $product_name
 * @property string $description
 * @property string $price
 * @property string $photos_url
 * @property bool $in_sell
 * @property bool $draft
 * @property int $total_score
 * @property string $seo_slug
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 *
 * @property Admin $createdBy
 * @property Admin $updatedBy
 * @property ProductCategory $productCategory
 * @property ProductOrderDetails[] $productOrderDetails
 * @property ProductReview[] $productReviews
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_category_id', 'total_score', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['product_category_id', 'total_score', 'created_by', 'updated_by'], 'integer'],
            [['product_name', 'description', 'price', 'seo_slug'], 'string'],
            [['product_category_id','product_subcategory_id', 'product_name', 'description', 'price'], 'required', 'message' => 'Este campo no puede estar vacío.'],
            [['in_sell', 'draft'], 'boolean'],
            [
                'photos_url', 'image',
                'skipOnEmpty' => true,
                'extensions' => 'jpg, jpeg, gif, png',
                'maxSize' => 5242880,
                'maxFiles' => 4,
                // 'minWidth' => 500,
                // 'maxWidth' => 1366,
                // 'minHeight' => 500,
                // 'maxHeight' => 900,

            ],
            //['photos_url', 'required', 'message' => 'Agregue por lo menos una imagen.'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['product_category_id' => 'id']],
            [['product_subcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSubcategory::className(), 'targetAttribute' => ['product_subcategory_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
                ],
                'value' => new Expression('NOW()')
            ],
            /* SeoSLUG */
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'product_name',
                'slugAttribute' => 'seo_slug'
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_category_id' => 'Product Category ID',
            'product_subcategory_id' => 'Product subCategory ID',
            'product_name' => 'Nombre del Producto',
            'description' => 'Descripción',
            'price' => 'Precio',
            'photos_url' => 'Fotos',
            'in_sell' => 'Disponibilidad',
            'draft' => 'Guardar como Borrador',
            'total_score' => 'Puntuación',
            'seo_slug' => 'Seo Slug',
            'created_by' => 'Creado Por',
            'created_at' => 'Fecha Creación',
            'updated_by' => 'Actualizado Por',
            'updated_at' => 'Fecha Actualización',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Admin::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Admin::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductOrderDetails()
    {
        return $this->hasMany(ProductOrderDetails::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductReviews()
    {
        return $this->hasMany(ProductReview::className(), ['product_id' => 'id']);
    }
}
