<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_subcategory".
 *
 * @property int $id
 * @property int $product_category_id
 * @property string $product_subcategory_name
 *
 * @property Product[] $products
 * @property ProductCategory $productCategory
 */
class ProductSubcategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_subcategory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_category_id'], 'default', 'value' => null],
            [['product_category_id'], 'integer'],
            [['product_category_id', 'product_subcategory_name'], 'required', 'message' => 'Este campo no puede estar vacío.'],
            [['product_subcategory_name'], 'string'],
            [['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['product_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_category_id' => 'Product Category ID',
            'product_subcategory_name' => 'Product Subcategory Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['product_subcategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }
}
