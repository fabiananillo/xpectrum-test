<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_product_payment".
 *
 * @property int $id
 * @property string $product_json
 * @property int $country_id
 * @property int $state_id
 * @property string $city
 * @property string $address
 * @property string $apartment
 * @property string $zip_code
 * @property string $phone
 * @property string $order_notes
 *
 * @property Payment[] $payments
 */
class OrderProductPayment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_product_payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_json', 'city', 'address', 'apartment', 'zip_code', 'phone', 'order_notes'], 'string'],
            [['country_id', 'state_id'], 'default', 'value' => null],
            [['country_id', 'state_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_json' => 'Product Json',
            'country_id' => 'Country ID',
            'state_id' => 'State ID',
            'city' => 'City',
            'address' => 'Address',
            'apartment' => 'Apartment',
            'zip_code' => 'Zip Code',
            'phone' => 'Phone',
            'order_notes' => 'Order Notes',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['order_product_payment_id' => 'id']);
    }
}
