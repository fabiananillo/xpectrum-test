<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $payment_id
 * @property int $user_id
 * @property string $total_payment
 * @property string $invoice
 * @property string $first_name
 * @property string $last_name
 * @property int $country_id
 * @property int $state_id
 * @property string $city
 * @property string $address
 * @property string $apartment
 * @property string $zip_code
 * @property string $email
 * @property string $phone
 * @property string $order_notes
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Countries $country
 * @property Payment $payment
 * @property States $state
 * @property User $user
 * @property OrderProductDetail[] $orderProductDetails
 */
class Order extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
                ],
                'value' => new Expression('NOW()')
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_id', 'user_id', 'country_id', 'state_id'], 'default', 'value' => null],
            [['payment_id', 'user_id', 'country_id', 'state_id'], 'integer'],
            [['total_payment', 'invoice', 'first_name', 'last_name', 'city', 'address', 'apartment', 'zip_code', 'email', 'phone', 'order_notes'], 'string'],
            [['first_name', 'last_name', 'country_id', 'state_id', 'city', 'address', 'zip_code', 'email', 'phone'], 'required', 'message' => 'Este campo no puede estar vacío.'],
            [['created_at', 'updated_at'], 'safe'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['payment_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_id' => 'Payment ID',
            'user_id' => 'User ID',
            'total_payment' => 'Total Payment',
            'invoice' => 'Invoice',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'country_id' => 'Country ID',
            'state_id' => 'State ID',
            'city' => 'City',
            'address' => 'Address',
            'apartment' => 'Apartment',
            'zip_code' => 'Zip Code',
            'email' => 'Email',
            'phone' => 'Phone',
            'order_notes' => 'Order Notes',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProductDetails()
    {
        return $this->hasMany(OrderProductDetail::className(), ['order_id' => 'id']);
    }
}
