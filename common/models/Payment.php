<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property string $concept
 * @property string $transfer_method
 * @property string $cardnumber
 * @property string $status
 * @property string $doc_type
 * @property string $document
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $value
 * @property string $ip_address
 * @property string $epayco_ref
 * @property string $created_at
 * @property string $updated_at
 * @property int $order_product_payment_id
 *
 * @property Order[] $orders
 * @property OrderProductPayment $orderProductPayment
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['concept', 'transfer_method', 'cardnumber', 'status', 'doc_type', 'document', 'name', 'last_name', 'email', 'value', 'ip_address', 'epayco_ref'], 'string'],
            [['value'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['order_product_payment_id'], 'default', 'value' => null],
            [['order_product_payment_id'], 'integer'],
            [['order_product_payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderProductPayment::className(), 'targetAttribute' => ['order_product_payment_id' => 'id']],
        ];
    }

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'concept' => 'Concept',
            'transfer_method' => 'Transfer Method',
            'cardnumber' => 'Cardnumber',
            'status' => 'Status',
            'doc_type' => 'Doc Type',
            'document' => 'Document',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'value' => 'Value',
            'ip_address' => 'Ip Address',
            'epayco_ref' => 'Epayco Ref',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'order_product_payment_id' => 'Order Product Payment ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProductPayment()
    {
        return $this->hasOne(OrderProductPayment::className(), ['id' => 'order_product_payment_id']);
    }
}
