<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $sortname
 * @property string $name
 * @property string $phonecode
 *
 * @property order[] $orders
 * @property States[] $states
 */
class Countries extends \yii\db\ActiveRecord
{

    const COLOMBIA_ID = 47;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sortname', 'name', 'phonecode'], 'string'],
        ];
    }

    public static function getAll()
    {
        $data = static::find()->all();
        $value = (count($data) == 0) ? ['' => ''] : \yii\helpers\ArrayHelper::map($data, 'id', 'name');

        return $value;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sortname' => 'Sortname',
            'name' => 'Name',
            'phonecode' => 'Phonecode',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getorders()
    {
        return $this->hasMany(order::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStates()
    {
        return $this->hasMany(States::className(), ['country_id' => 'id']);
    }
}
