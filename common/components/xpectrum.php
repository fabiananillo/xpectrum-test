<?php

/* Colocar esto para usar componentes */

namespace common\components;

use common\models\Countries;
use common\models\OrderProductDetail;
use common\models\Payment;
use common\models\Product;
use Yii;
use yii\base\Component;
use yii\db\Query;

/**
 *
 * @author fabian
 * 
 * 
 */
class xpectrum extends Component
{

    /*
        cloudinary settings
    */

    const cloudinarySettings = [
        "cloud_name" => "fabiananillo",
        "api_key" => "769893857249793",
        "api_secret" => "o7HxqhKxrmm2V_Cmp1KaTAg_QjU",
        "secure" => true
    ];


    public function uploadImage($imagePath)
    {
        \Cloudinary::config(static::cloudinarySettings);

        if ($uploadImage = \Cloudinary\Uploader::upload($imagePath)) {
            return $uploadImage['secure_url'];
        }
    }

    /**
     * @param array $image The object instance from image to scale
     * @param string $path Current path
     * @param int $width Image width 
     * @param int $height Image height
     * @param int $quality Image quality [0 - 100]
     * 
     * This function resize the image from a temp file, so it recieves the temp file 
     */
    public function scaleImage($image, $path, $folder, $width, $height, $quality)
    {
        $imageResize = Yii::$app->image->load($path . $image->name);
        $imageResize->resize($width, $height);
        $imageResize->save($file = $path . $folder . $image->name, $quality);
    }

    public function countStarsByTotalScore($totalScore)
    {
        switch ($totalScore) {
            case ($totalScore >= 1 && $totalScore <= 1.9):
                return '<i class="fas fa-star" style="color:#fdd003;"></i>';
            case ($totalScore >= 2 && $totalScore <= 2.9):
                return '<i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i>';
            case ($totalScore >= 3 && $totalScore <= 3.9):
                return '<i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i>';
            case ($totalScore >= 4 && $totalScore <= 4.9):
                return '<i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i>';
            case ($totalScore >= 5):
                return '<i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i><i class="fas fa-star" style="color:#fdd003;"></i>';
        }
    }

    public function formatMediaUrl($urlString)
    {
        $result = array();
        $keys = array('{', '}');
        $explode = explode(",", str_replace($keys, '', $urlString));
        array_push($result, $explode);

        return $result[0];
    }

    public function getProductInformationCart($productId)
    {
        $cart = Yii::$app->cart;

        return $cart->getItem($productId);
    }

    public function getCountProductsCart()
    {
        $cart = Yii::$app->cart;
        return $cart->getTotalCount();
    }

    public function getCountryNameById($id)
    {
        return (new Query())
            ->select('name')
            ->from('countries')
            ->where(['id' => $id])
            ->one()['name'];
    }

    public function getStateNameById($id)
    {
        return (new Query())
            ->select('name')
            ->from('states')
            ->where(['id' => $id])
            ->one()['name'];
    }

    public function getOrderProductDetail($id)
    {
        return OrderProductDetail::find()
            ->where(['order_id' => $id])
            ->all();
    }

    public function getProductInformationById($id)
    {
        return Product::find()
            ->where(['id' => $id])
            ->one();
    }

    public function getPaymentInformation($id)
    {
        return Payment::findOne(['id' => $id]);
    }
}
