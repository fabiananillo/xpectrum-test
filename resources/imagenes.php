<div class="alert alert-info">  
    
        <?= $form->field($projectImage, 'imagen[]')->widget(FileInput::className(),[
            'name' => 'Imagenes',
            'options'=>[
                'multiple'=>true,
             
            ],
            'pluginOptions' => [
                'showCaption' => false,
                'showRemove' => true,
                'showUpload' => false,
                'uploadUrl' => Url::to(['gestionar-proyecto']),
               
                'maxFileCount' => 15
            ]
        ]);       
        ?>