 <?=
    PopoverX::widget([
        'header' => 'Mensajes',
        'placement' => PopoverX::ALIGN_BOTTOM,
        'content' => $notify,
        'footer' => Html::button('Aceptar', ['class'=>'btn btn-sm btn-primary']),
        'toggleButton' => ['label'=>'Información', 'class'=>'btn btn-default'],
    ]);
?>